// logger.cpp : programa logger
// by Rodrigo Garcia Lucio	50183
////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#define MAX_DATOS 128


int main (int argc,char* argv[]){

	int fd, leido;
	char buffer[MAX_DATOS];
	char fin[4]="FIN";

	if( mkfifo("/tmp/my_fifo", 0666)==-1 ){
		perror("Error al crear fifo\n");
		unlink("/tmp/my_fifo");
		exit(1);
	}


	fd = open("/tmp/my_fifo", O_RDONLY);
	if(fd==-1){
		perror("Error al abrir fifo en logger\n");
		close(fd);
		unlink("/tmp/my_fifo");
		exit(1);
	}


	while(1){
		leido=read(fd, &buffer, MAX_DATOS);
		if(leido==-1){
			close(fd);
			perror("Error al leer fifo\n");
			exit(1);
		}

		if(strcmp(buffer, fin)==0) break;

		write(1, &buffer, leido);

	}

	write(1,"fin de juego\n", strlen("fin del juego\n"));
	close(fd);
	unlink("/tmp/my_fifo");


	return 0;
}
