// Mundo.cpp: implementation of the CMundo class.
// by Rodrigo Garcia Lucio	50183
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	//cerrar tuberia con logger
	write(fd_tuberia,"FIN",strlen("FIN")+1);
	close(fd_tuberia);

	//desproyectar
	error_desproy=munmap(dir_mem, sizeof(*dir_mem));
	if(error_desproy==-1){
		perror("error en la desproyeccion de mundo");
		exit(1);
	}
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;

		//ENVIO GOL DE PLAYER2 A LOGGER
		if(puntos1>puntos2)
			sprintf(frase, "Jugador 2 marca gol. Marcador: %d-%d \nGana Jugador 1.\n\n", puntos1, puntos2);
		else if(puntos1<puntos2)
			sprintf(frase, "Jugador 2 marca gol. Marcador: %d-%d \nGana Jugador 2.\n\n", puntos1, puntos2);
		else
			sprintf(frase, "Jugador 2 marca gol. Marcador: %d-%d \nEmpate.\n\n", puntos1, puntos2);

		write(fd_tuberia, &frase , strlen(frase) );

	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		//ENVIO GOL DE PLAYER1 A LOGGER
		if(puntos1>puntos2)
                        sprintf(frase, "Jugador 1 marca gol. Marcador: %d-%d \nGana Jugador 1.\n\n",  puntos1, puntos2);
                else if(puntos1<puntos2)
                        sprintf(frase, "Jugador 1 marca gol. Marcador: %d-%d \nGana Jugador 2.\n\n", puntos1, puntos2);
                else
                        sprintf(frase, "Jugador 1 marca gol. Marcador: %d-%d \nEmpate.\n\n", puntos1, puntos2);


                write(fd_tuberia, &frase , strlen(frase) );

	}

// codigo para el bot

	p_memoria->esfera = esfera;
	p_memoria->raqueta1 = jugador1;

	if(p_memoria->accion == 1)		//sera mejor son un switch??
		OnKeyboardDown('w', 0, 0);	//los atributos x e y con 0s ??
	else if(p_memoria->accion == -1)
		OnKeyboardDown('s', 0, 0);
	else
		jugador1.velocidad.y=0;		//otra opcion??


	//terminar el juego xq jugador llego a 3 puntos
	if(puntos1>=3 | puntos2>=3){
		p_memoria->accion = 2;

		if(puntos1==3)
			sprintf(frase, "\tGANA JUGADOR 1 \n\n----------GAME OVER-----------\n\n", 1);
		else
			sprintf(frase, "\tGANA JUGADOR 2 \n\n----------GAME OVER-----------\n\n", 2);

		write(fd_tuberia, &frase, strlen(frase) );

		exit(0);
	}

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;


//ABRO FIFO PARA ESCRITURA
	fd_tuberia = open("/tmp/my_fifo", O_WRONLY);
	if(fd_tuberia==-1){
		perror("Error al abrir fifo en mundo\n");
		close(fd_tuberia);
	}

//codigo para el bot

	//char *dir_mem;
	//int fd_comp;

	fd_comp=open("/tmp/compartida.txt", O_CREAT|O_TRUNC|O_RDWR, 0666);
	if(fd_comp==-1){
		perror("error al abrir fichero compartido");
		exit(1);
	}

	write(fd_comp, &memoria , sizeof(memoria));	//porque este write?

	dir_mem=(char*)mmap(NULL, sizeof(memoria) , PROT_READ|PROT_WRITE, MAP_SHARED, fd_comp, 0);
	if(dir_mem == MAP_FAILED){
		perror("error al proyectar en mundo");
		close(fd_comp);
		exit(1);
	}

	if(close(fd_comp)==-1){
		perror("error al cerrar fichero compartido");
		munmap(dir_mem, sizeof(*dir_mem));
		exit(1);
	}

	p_memoria=(DatosMemCompartida*)dir_mem;	//esto es asi?? porque??

}
