// Esfera.cpp: implementation of the Esfera class.
// by Rodrigo Garcia Lucio	50183
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=1.0f;
	velocidad.x=3;
	velocidad.y=3;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro.x += velocidad.x * t;
	centro.y += velocidad.y * t;

	if(radio>0.2)
		radio -= 0.05*t;

	if(radio<=0.2)
		radio = 1.0;
}
