// bot.cpp : progama bot
// by Rodrigo Garcia Lucio	50183
//////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <DatosMemCompartida.h>


int main(int argc, char* argv[]){

	DatosMemCompartida* datos;
	char* dir_mem;
	int fd, error_desproy;
	float pos_bot;

//abrir fichero
	fd=open("/tmp/compartida.txt", O_RDWR);
	if(fd==-1){
		perror("error al abrir fichero compartido en bot");
		exit(1);
	}

//proyectar
	dir_mem =(char*)mmap(NULL, sizeof(*datos), PROT_READ|PROT_WRITE , MAP_SHARED , fd, 0);
	if(dir_mem == MAP_FAILED){
		perror("error al proyectar en bot");
		close(fd);
		exit(1);
	}

//cerrrar el fd
	if(close(fd)==-1){
		perror("error al cerrar fichero compartido en bot");
		munmap(dir_mem, sizeof(*dir_mem));
		exit(1);
	}


	datos =(DatosMemCompartida*)dir_mem;

	while(1){
		pos_bot = (datos->raqueta1.y1 + datos->raqueta1.y2)/2;

		if(pos_bot < (datos->esfera.centro.y))
			datos->accion = 1;
		else if(pos_bot > (datos->esfera.centro.y))
			datos->accion = -1;
		else
			datos->accion = 0;

		usleep(2500);

		//salida del bucle
		if(datos->accion == 2) break;

	}

//desproyectar
	error_desproy=munmap(dir_mem, sizeof(*dir_mem));	//es con fstat??
	if(error_desproy==-1){
		perror("error en la desproyeccion del bot");
		exit(1);
	}

	return 0;
}
