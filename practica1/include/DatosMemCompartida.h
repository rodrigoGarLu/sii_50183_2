// DatosMemCompartida.h : interface for DatosMemCompartida class
// by Rodrigo Garcia Lucio	50183
////////////////////////////////////////////////

#pragma once
#include "Esfera.h"
#include "Raqueta.h"

class DatosMemCompartida
{
public:
	Esfera esfera;
	Raqueta raqueta1;

	int accion; //1 arriba, 0 nada, -1 abajo


};
